"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopListService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const shop_list_entity_1 = require("./shop-list.entity");
let ShopListService = class ShopListService {
    constructor(shopListRepository) {
        this.shopListRepository = shopListRepository;
    }
    async create(data) {
        return await this.shopListRepository.save(data);
    }
    async update(id, data) {
        return await this.shopListRepository.save(Object.assign({ id: id }, data));
    }
    findAll() {
        return this.shopListRepository.find({ relations: ['ingredient', 'unit'] });
    }
    findOne(id) {
        return this.shopListRepository.findOne(id, { relations: ['ingredient', 'unit'] });
    }
    findByUserAndIngredient(userId, ingredientId) {
        let result = this.shopListRepository
            .createQueryBuilder('shopList')
            .leftJoinAndSelect('shopList.ingredient', 'ingredient')
            .leftJoinAndSelect('shopList.unit', 'unit')
            .leftJoinAndSelect('shopList.userList', 'userList')
            .where({ userList: { userId: userId }, ingredient: { id: ingredientId } })
            .getOne();
        return result;
    }
    async remove(id) {
        await this.shopListRepository.delete(id);
    }
};
ShopListService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(shop_list_entity_1.ShopList)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], ShopListService);
exports.ShopListService = ShopListService;
//# sourceMappingURL=shop-list.service.js.map