import { UserList } from './../user-list/user-list.entity';
import { Unit } from './../units/unit.entity';
import { Ingredient } from './../ingredients/ingredient.entity';
export declare class ShopList {
    id: number;
    quantity: number;
    userList: UserList;
    ingredient: Ingredient;
    unit: Unit;
    checked: boolean;
}
