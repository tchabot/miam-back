"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopListModule = void 0;
const shop_list_entity_1 = require("./shop-list.entity");
const common_1 = require("@nestjs/common");
const shop_list_service_1 = require("./shop-list.service");
const shop_list_controller_1 = require("./shop-list.controller");
const typeorm_1 = require("@nestjs/typeorm");
let ShopListModule = class ShopListModule {
};
ShopListModule = __decorate([
    common_1.Module({
        providers: [shop_list_service_1.ShopListService],
        imports: [typeorm_1.TypeOrmModule.forFeature([shop_list_entity_1.ShopList])],
        controllers: [shop_list_controller_1.ShopListController],
        exports: [shop_list_service_1.ShopListService]
    })
], ShopListModule);
exports.ShopListModule = ShopListModule;
//# sourceMappingURL=shop-list.module.js.map