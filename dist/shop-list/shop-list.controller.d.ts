import { ShopListService } from './shop-list.service';
import { ShopList } from './shop-list.entity';
import { CreateShopListDto } from './dto/create-shopList.dto';
import { UpdateShopListDto } from './dto/update-shopList.dto';
export declare class ShopListController {
    private readonly shopListService;
    constructor(shopListService: ShopListService);
    findAll(query: any): Promise<ShopList[]> | Promise<ShopList>;
    create(shopList: CreateShopListDto): Promise<ShopList>;
    findOne(id: number): Promise<ShopList>;
    update(id: string, updateShopListDto: UpdateShopListDto): Promise<ShopList>;
    delete(id: number): Promise<void>;
}
