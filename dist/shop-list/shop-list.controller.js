"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopListController = void 0;
const jwt_auth_guard_1 = require("./../auth/jwt-auth.guard");
const common_1 = require("@nestjs/common");
const shop_list_service_1 = require("./shop-list.service");
const create_shopList_dto_1 = require("./dto/create-shopList.dto");
const update_shopList_dto_1 = require("./dto/update-shopList.dto");
let ShopListController = class ShopListController {
    constructor(shopListService) {
        this.shopListService = shopListService;
    }
    findAll(query) {
        if (query.userId && query.ingredientId) {
            return this.shopListService.findByUserAndIngredient(query.userId, query.ingredientId);
        }
        else {
            return this.shopListService.findAll();
        }
    }
    create(shopList) {
        return this.shopListService.create(shopList);
    }
    findOne(id) {
        return this.shopListService.findOne(id);
    }
    update(id, updateShopListDto) {
        return this.shopListService.update(parseInt(id), updateShopListDto);
    }
    delete(id) {
        return this.shopListService.remove(id);
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ShopListController.prototype, "findAll", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_shopList_dto_1.CreateShopListDto]),
    __metadata("design:returntype", Promise)
], ShopListController.prototype, "create", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ShopListController.prototype, "findOne", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_shopList_dto_1.UpdateShopListDto]),
    __metadata("design:returntype", Promise)
], ShopListController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], ShopListController.prototype, "delete", null);
ShopListController = __decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Controller('shop-list'),
    __metadata("design:paramtypes", [shop_list_service_1.ShopListService])
], ShopListController);
exports.ShopListController = ShopListController;
//# sourceMappingURL=shop-list.controller.js.map