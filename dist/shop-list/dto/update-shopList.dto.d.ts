export declare class UpdateShopListDto {
    id: number;
    checked: boolean;
    quantity: number;
}
