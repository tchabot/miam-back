import { Unit } from './../../units/unit.entity';
import { Ingredient } from './../../ingredients/ingredient.entity';
import { User } from './../../users/user.entity';
export declare class CreateShopListDto {
    user: User;
    ingredient: Ingredient;
    unit: Unit;
    checked: boolean;
    quantity: number;
}
