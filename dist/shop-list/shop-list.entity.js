"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopList = void 0;
const user_list_entity_1 = require("./../user-list/user-list.entity");
const unit_entity_1 = require("./../units/unit.entity");
const ingredient_entity_1 = require("./../ingredients/ingredient.entity");
const typeorm_1 = require("typeorm");
let ShopList = class ShopList {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], ShopList.prototype, "id", void 0);
__decorate([
    typeorm_1.Column({ nullable: true }),
    __metadata("design:type", Number)
], ShopList.prototype, "quantity", void 0);
__decorate([
    typeorm_1.ManyToOne(type => user_list_entity_1.UserList, userList => userList.shopLists),
    __metadata("design:type", user_list_entity_1.UserList)
], ShopList.prototype, "userList", void 0);
__decorate([
    typeorm_1.ManyToOne(type => ingredient_entity_1.Ingredient, ingredient => ingredient.shopLists),
    __metadata("design:type", ingredient_entity_1.Ingredient)
], ShopList.prototype, "ingredient", void 0);
__decorate([
    typeorm_1.ManyToOne(type => unit_entity_1.Unit, unit => unit.shopLists),
    __metadata("design:type", unit_entity_1.Unit)
], ShopList.prototype, "unit", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", Boolean)
], ShopList.prototype, "checked", void 0);
ShopList = __decorate([
    typeorm_1.Entity()
], ShopList);
exports.ShopList = ShopList;
//# sourceMappingURL=shop-list.entity.js.map