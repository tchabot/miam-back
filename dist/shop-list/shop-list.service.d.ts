import { CreateShopListDto } from './dto/create-shopList.dto';
import { Repository } from 'typeorm';
import { ShopList } from './shop-list.entity';
export declare class ShopListService {
    private shopListRepository;
    constructor(shopListRepository: Repository<ShopList>);
    create(data: CreateShopListDto): Promise<ShopList>;
    update(id: number, data: Object): Promise<ShopList>;
    findAll(): Promise<ShopList[]>;
    findOne(id: number): Promise<ShopList>;
    findByUserAndIngredient(userId: number, ingredientId: number): Promise<ShopList>;
    remove(id: number): Promise<void>;
}
