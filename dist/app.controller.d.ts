import { User } from './users/user.entity';
import { AuthService } from './auth/auth.service';
import { UsersService } from './users/users.service';
export declare class AppController {
    private authService;
    private usersService;
    constructor(authService: AuthService, usersService: UsersService);
    login(user: User): Promise<{
        access_token: string;
    }>;
    register(user: User): Promise<User>;
    getProfile(req: any): Promise<User>;
}
