"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const user_list_entity_1 = require("./user-list.entity");
let UserListService = class UserListService {
    constructor(userListRepository) {
        this.userListRepository = userListRepository;
    }
    async create(data) {
        return await this.userListRepository.save(data);
    }
    async update(id, data) {
        return await this.userListRepository.save(Object.assign({ id: id }, data));
    }
    findAll() {
        return this.userListRepository.find({ order: { title: 'ASC' } });
    }
    findOne(id) {
        return this.userListRepository.findOne(id);
    }
    findByUserId(id) {
        return this.userListRepository
            .createQueryBuilder('userList')
            .leftJoinAndSelect('userList.shopLists', 'shopLists')
            .leftJoinAndSelect('shopLists.ingredient', 'ingredient')
            .where({ user: { id: id } })
            .orderBy('userList.title', 'ASC')
            .getMany();
    }
    async remove(id) {
        await this.userListRepository.delete(id);
    }
};
UserListService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(user_list_entity_1.UserList)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], UserListService);
exports.UserListService = UserListService;
//# sourceMappingURL=user-list.service.js.map