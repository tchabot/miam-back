import { User } from './../../users/user.entity';
export declare class CreateUserListDto {
    user: User;
    title: string;
}
