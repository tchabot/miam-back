import { User } from 'src/users/user.entity';
export declare class UpdateUserListDto {
    id: number;
    user: User;
    title: string;
}
