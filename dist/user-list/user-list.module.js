"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListModule = void 0;
const user_list_entity_1 = require("./user-list.entity");
const user_list_service_1 = require("./user-list.service");
const common_1 = require("@nestjs/common");
const user_list_controller_1 = require("./user-list.controller");
const typeorm_1 = require("@nestjs/typeorm");
let UserListModule = class UserListModule {
};
UserListModule = __decorate([
    common_1.Module({
        controllers: [user_list_controller_1.UserListController],
        providers: [user_list_service_1.UserListService],
        imports: [typeorm_1.TypeOrmModule.forFeature([user_list_entity_1.UserList])],
        exports: [user_list_service_1.UserListService],
    })
], UserListModule);
exports.UserListModule = UserListModule;
//# sourceMappingURL=user-list.module.js.map