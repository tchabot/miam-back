import { Repository } from 'typeorm';
import { UserList } from './user-list.entity';
export declare class UserListService {
    private userListRepository;
    constructor(userListRepository: Repository<UserList>);
    create(data: any): Promise<UserList>;
    update(id: number, data: Object): Promise<UserList>;
    findAll(): Promise<UserList[]>;
    findOne(id: number): Promise<UserList>;
    findByUserId(id: number): Promise<UserList[]>;
    remove(id: number): Promise<void>;
}
