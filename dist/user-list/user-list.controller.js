"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserListController = void 0;
const jwt_auth_guard_1 = require("./../auth/jwt-auth.guard");
const common_1 = require("@nestjs/common");
const user_list_service_1 = require("./user-list.service");
const create_user_list_dto_1 = require("./dto/create-user-list.dto");
const update_user_list_dto_1 = require("./dto/update-user-list.dto");
let UserListController = class UserListController {
    constructor(userListService) {
        this.userListService = userListService;
    }
    findAll(query) {
        if (query.userId) {
            return this.userListService.findByUserId(query.userId);
        }
        else {
            return this.userListService.findAll();
        }
    }
    create(userList) {
        return this.userListService.create(userList);
    }
    findOne(id) {
        return this.userListService.findOne(id);
    }
    update(id, updateUserListDto) {
        return this.userListService.update(parseInt(id), updateUserListDto);
    }
    delete(id) {
        return this.userListService.remove(id);
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserListController.prototype, "findAll", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_user_list_dto_1.CreateUserListDto]),
    __metadata("design:returntype", Promise)
], UserListController.prototype, "create", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserListController.prototype, "findOne", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_user_list_dto_1.UpdateUserListDto]),
    __metadata("design:returntype", Promise)
], UserListController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserListController.prototype, "delete", null);
UserListController = __decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Controller('user-list'),
    __metadata("design:paramtypes", [user_list_service_1.UserListService])
], UserListController);
exports.UserListController = UserListController;
//# sourceMappingURL=user-list.controller.js.map