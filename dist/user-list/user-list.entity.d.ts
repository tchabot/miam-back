import { User } from './../users/user.entity';
import { ShopList } from 'src/shop-list/shop-list.entity';
export declare class UserList {
    id: number;
    title: string;
    user: User;
    shopLists: ShopList[];
}
