import { UserListService } from './user-list.service';
import { UserList } from './user-list.entity';
import { CreateUserListDto } from './dto/create-user-list.dto';
import { UpdateUserListDto } from './dto/update-user-list.dto';
export declare class UserListController {
    private readonly userListService;
    constructor(userListService: UserListService);
    findAll(query: any): Promise<UserList[]> | Promise<UserList>;
    create(userList: CreateUserListDto): Promise<UserList>;
    findOne(id: number): Promise<UserList>;
    update(id: string, updateUserListDto: UpdateUserListDto): Promise<UserList>;
    delete(id: number): Promise<void>;
}
