import { Repository } from 'typeorm';
import { Unit } from './unit.entity';
export declare class UnitsService {
    private unitRepository;
    constructor(unitRepository: Repository<Unit>);
    create(data: any): Promise<Unit>;
    update(id: number, data: Object): Promise<Unit>;
    findAll(): Promise<Unit[]>;
    findOne(id: number): Promise<Unit>;
    remove(id: number): Promise<void>;
}
