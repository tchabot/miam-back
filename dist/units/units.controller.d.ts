import { UnitsService } from './units.service';
import { Unit } from './unit.entity';
import { UpdateUnitDto } from './dto/update-unit.dto';
export declare class UnitsController {
    private readonly unitsService;
    constructor(unitsService: UnitsService);
    findAll(): Promise<Unit[]>;
    create(unit: Unit): Promise<Unit>;
    findOne(id: number): Promise<Unit>;
    update(id: string, updateUnitDto: UpdateUnitDto): Promise<Unit>;
    delete(id: number): Promise<void>;
}
