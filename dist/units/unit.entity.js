"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Unit = void 0;
const shop_list_entity_1 = require("../shop-list/shop-list.entity");
const typeorm_1 = require("typeorm");
let Unit = class Unit {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Unit.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Unit.prototype, "name", void 0);
__decorate([
    typeorm_1.OneToMany(type => shop_list_entity_1.ShopList, shopLists => shopLists.unit),
    __metadata("design:type", Array)
], Unit.prototype, "shopLists", void 0);
Unit = __decorate([
    typeorm_1.Entity()
], Unit);
exports.Unit = Unit;
//# sourceMappingURL=unit.entity.js.map