import { ShopList } from "src/shop-list/shop-list.entity";
export declare class Unit {
    id: number;
    name: string;
    shopLists: ShopList[];
}
