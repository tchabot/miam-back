import { Recipe } from './recipe.entity';
import { Repository } from 'typeorm';
export declare class RecipesService {
    private recipeRepository;
    constructor(recipeRepository: Repository<Recipe>);
    create(data: any): Promise<Recipe>;
    update(id: number, data: Object): Promise<Recipe>;
    findAll(): Promise<Recipe[]>;
    findOne(id: number): Promise<Recipe>;
    remove(id: number): Promise<void>;
}
