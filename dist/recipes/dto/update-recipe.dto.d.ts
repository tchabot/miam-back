export declare class UpdateRecipeDto {
    id: number;
    name: string;
    description: string;
    parts: number;
}
