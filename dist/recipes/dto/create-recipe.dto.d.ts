import { Category } from './../../categories/category.entity';
export declare class CreateRecipeDto {
    name: string;
    description: string;
    parts: number;
    category: Category;
}
