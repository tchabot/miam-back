import { CreateRecipeDto } from './dto/create-recipe.dto';
import { RecipesService } from './recipes.service';
import { Recipe } from './recipe.entity';
import { UpdateRecipeDto } from './dto/update-recipe.dto';
export declare class RecipesController {
    private readonly recipesService;
    constructor(recipesService: RecipesService);
    findAll(): Promise<Recipe[]>;
    create(recipe: CreateRecipeDto): Promise<Recipe>;
    findOne(id: number): Promise<Recipe>;
    update(id: string, updateRecipeDto: UpdateRecipeDto): Promise<Recipe>;
    delete(id: number): Promise<void>;
}
