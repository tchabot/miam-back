import { Category } from './../categories/category.entity';
export declare class Recipe {
    id: number;
    name: string;
    description: string;
    parts: number;
    categories: Category[];
}
