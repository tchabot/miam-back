import { UserList } from './../user-list/user-list.entity';
export declare class User {
    id: number;
    email: string;
    password: string;
    userLists: UserList[];
}
