"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ingredient = void 0;
const ColumnNumericTransformer_1 = require("./../ColumnNumericTransformer");
const typeorm_1 = require("typeorm");
const shop_list_entity_1 = require("../shop-list/shop-list.entity");
let Ingredient = class Ingredient {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Ingredient.prototype, "id", void 0);
__decorate([
    typeorm_1.Column(),
    __metadata("design:type", String)
], Ingredient.prototype, "name", void 0);
__decorate([
    typeorm_1.OneToMany(type => shop_list_entity_1.ShopList, shopLists => shopLists.ingredient),
    __metadata("design:type", Array)
], Ingredient.prototype, "shopLists", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "calories", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "water", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "proteins", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "glucids", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "lipids", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "sugars", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "starch", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "fibers", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "fatSaturated", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "fatMonoUnsaturated", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "fatPolyUnsaturated", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "cholesterol", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "salt", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "calcium", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "chlorure", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "copper", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "iron", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "iode", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "magnesium", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "manganese", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "phosophore", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "potassium", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "selenium", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "sodium", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "zinc", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "retinol", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "betaCarotene", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminD", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminE", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminK1", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminK2", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminC", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminB1", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminB2", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminB3", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminB5", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminB6", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminB9", void 0);
__decorate([
    typeorm_1.Column('decimal', {
        nullable: true,
        precision: 10,
        scale: 2,
        transformer: new ColumnNumericTransformer_1.ColumnNumericTransformer(),
    }),
    __metadata("design:type", Number)
], Ingredient.prototype, "vitaminB12", void 0);
Ingredient = __decorate([
    typeorm_1.Entity()
], Ingredient);
exports.Ingredient = Ingredient;
//# sourceMappingURL=ingredient.entity.js.map