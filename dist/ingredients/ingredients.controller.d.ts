import { IngredientsService } from './ingredients.service';
import { Ingredient } from './ingredient.entity';
import { UpdateIngredientDto } from './dto/update-ingredient.dto';
export declare class IngredientsController {
    private readonly ingredientsService;
    constructor(ingredientsService: IngredientsService);
    findAll(): Promise<Ingredient[]>;
    create(ingredient: Ingredient): Promise<Ingredient>;
    findOne(id: number): Promise<Ingredient>;
    update(id: string, updateIngredientDto: UpdateIngredientDto): Promise<Ingredient>;
    delete(id: number): Promise<void>;
}
