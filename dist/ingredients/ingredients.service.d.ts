import { Repository } from 'typeorm';
import { Ingredient } from './ingredient.entity';
export declare class IngredientsService {
    private ingredientRepository;
    constructor(ingredientRepository: Repository<Ingredient>);
    create(data: any): Promise<Ingredient>;
    update(id: number, data: Object): Promise<Ingredient>;
    findAll(): Promise<Ingredient[]>;
    findOne(id: number): Promise<Ingredient>;
    remove(id: number): Promise<void>;
}
