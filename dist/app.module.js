"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const cooks_module_1 = require("./cooks/cooks.module");
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const ingredients_module_1 = require("./ingredients/ingredients.module");
const recipes_module_1 = require("./recipes/recipes.module");
const shop_list_module_1 = require("./shop-list/shop-list.module");
const units_module_1 = require("./units/units.module");
const typeorm_1 = require("@nestjs/typeorm");
const auth_module_1 = require("./auth/auth.module");
const users_module_1 = require("./users/users.module");
const categories_module_1 = require("./categories/categories.module");
const user_list_module_1 = require("./user-list/user-list.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                host: '51.210.182.219',
                port: 5432,
                username: 'tchabot',
                password: 'toor*',
                database: 'miam',
                entities: ['dist/**/*.entity{.ts,.js}'],
                autoLoadEntities: true,
                synchronize: true,
            }),
            ingredients_module_1.IngredientsModule,
            recipes_module_1.RecipesModule,
            shop_list_module_1.ShopListModule,
            units_module_1.UnitsModule,
            auth_module_1.AuthModule,
            users_module_1.UsersModule,
            cooks_module_1.CooksModule,
            categories_module_1.CategoriesModule,
            user_list_module_1.UserListModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map