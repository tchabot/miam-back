"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CooksModule = void 0;
const cook_entity_1 = require("./cook.entity");
const cooks_service_1 = require("./cooks.service");
const cooks_controller_1 = require("./cooks.controller");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
let CooksModule = class CooksModule {
};
CooksModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([cook_entity_1.Cook])],
        controllers: [cooks_controller_1.CooksController],
        providers: [cooks_service_1.CooksService],
        exports: [cooks_service_1.CooksService],
    })
], CooksModule);
exports.CooksModule = CooksModule;
//# sourceMappingURL=cooks.module.js.map