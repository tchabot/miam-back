export declare class Cook {
    id: number;
    quantity: number;
    recipeId: number;
    ingredientId: number;
    unitId: number;
}
