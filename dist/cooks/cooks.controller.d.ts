import { CooksService } from './cooks.service';
import { Cook } from './cook.entity';
import { UpdateCookDto } from './dto/update-cook.dto';
export declare class CooksController {
    private readonly cooksService;
    constructor(cooksService: CooksService);
    findAll(query: any): Promise<Cook[]>;
    create(cook: Cook): Promise<Cook>;
    findOne(id: number): Promise<Cook>;
    update(id: string, updateCookDto: UpdateCookDto): Promise<Cook>;
    delete(id: number): Promise<void>;
}
