"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CooksService = void 0;
const cook_entity_1 = require("./cook.entity");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
let CooksService = class CooksService {
    constructor(cookRepository) {
        this.cookRepository = cookRepository;
    }
    async create(data) {
        return await this.cookRepository.save(data);
    }
    async update(id, data) {
        return await this.cookRepository.save(Object.assign({ id: id }, data));
    }
    findAll() {
        return this.cookRepository.find();
    }
    findOne(id) {
        return this.cookRepository.findOne(id);
    }
    findByRecipe(id) {
        return this.cookRepository
            .createQueryBuilder('cook')
            .where('cook.recipeId = :id', { id: id })
            .getMany();
    }
    async remove(id) {
        await this.cookRepository.delete(id);
    }
};
CooksService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(cook_entity_1.Cook)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], CooksService);
exports.CooksService = CooksService;
//# sourceMappingURL=cooks.service.js.map