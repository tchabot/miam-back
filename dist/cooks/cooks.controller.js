"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CooksController = void 0;
const jwt_auth_guard_1 = require("./../auth/jwt-auth.guard");
const common_1 = require("@nestjs/common");
const cooks_service_1 = require("./cooks.service");
const cook_entity_1 = require("./cook.entity");
const update_cook_dto_1 = require("./dto/update-cook.dto");
let CooksController = class CooksController {
    constructor(cooksService) {
        this.cooksService = cooksService;
    }
    findAll(query) {
        if (query.recipeId) {
            return this.cooksService.findByRecipe(query.recipeId);
        }
        else {
            return this.cooksService.findAll();
        }
    }
    create(cook) {
        return this.cooksService.create(cook);
    }
    findOne(id) {
        return this.cooksService.findOne(id);
    }
    update(id, updateCookDto) {
        return this.cooksService.update(parseInt(id), updateCookDto);
    }
    delete(id) {
        return this.cooksService.remove(id);
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], CooksController.prototype, "findAll", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [cook_entity_1.Cook]),
    __metadata("design:returntype", Promise)
], CooksController.prototype, "create", null);
__decorate([
    common_1.Get(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CooksController.prototype, "findOne", null);
__decorate([
    common_1.Put(':id'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_cook_dto_1.UpdateCookDto]),
    __metadata("design:returntype", Promise)
], CooksController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CooksController.prototype, "delete", null);
CooksController = __decorate([
    common_1.UseGuards(jwt_auth_guard_1.JwtAuthGuard),
    common_1.Controller('cooks'),
    __metadata("design:paramtypes", [cooks_service_1.CooksService])
], CooksController);
exports.CooksController = CooksController;
//# sourceMappingURL=cooks.controller.js.map