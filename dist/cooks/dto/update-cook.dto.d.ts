export declare class UpdateCookDto {
    id: number;
    recipeId: number;
    ingredientId: number;
    unitId: number;
}
