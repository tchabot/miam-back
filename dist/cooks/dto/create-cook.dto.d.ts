export declare class CreateCookDto {
    recipeId: number;
    ingredientId: number;
    unitId: number;
}
