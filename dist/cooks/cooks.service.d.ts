import { Cook } from './cook.entity';
import { Repository } from 'typeorm';
export declare class CooksService {
    private cookRepository;
    constructor(cookRepository: Repository<Cook>);
    create(data: any): Promise<Cook>;
    update(id: number, data: Object): Promise<Cook>;
    findAll(): Promise<Cook[]>;
    findOne(id: number): Promise<Cook>;
    findByRecipe(id: number): Promise<Cook[]>;
    remove(id: number): Promise<void>;
}
