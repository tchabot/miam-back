import { ShopList } from './../shop-list/shop-list.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class Cook {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  quantity: number;

  @Column()
  recipeId: number;

  @Column()
  ingredientId: number;

  @Column({ nullable: true })
  unitId: number;
}
