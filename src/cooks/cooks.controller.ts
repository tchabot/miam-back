import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import {
  Controller,
  Get,
  Delete,
  Body,
  Put,
  Param,
  UseGuards,
  Post,
  Query,
} from '@nestjs/common';
import { CooksService } from './cooks.service';
import { Cook } from './cook.entity';
import { UpdateCookDto } from './dto/update-cook.dto';

@UseGuards(JwtAuthGuard)
@Controller('cooks')
export class CooksController {
  constructor(private readonly cooksService: CooksService) {}

  @Get()
  findAll(@Query() query: any): Promise<Cook[]> {

    if (query.recipeId) {
      return this.cooksService.findByRecipe(query.recipeId);
    } else {
      return this.cooksService.findAll();
    }
  }

  @Post()
  create(@Body() cook: Cook): Promise<Cook> {
    return this.cooksService.create(cook);
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Cook> {
    return this.cooksService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateCookDto: UpdateCookDto,
  ): Promise<Cook> {
    return this.cooksService.update(parseInt(id), updateCookDto);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<void> {
    return this.cooksService.remove(id);
  }
}
