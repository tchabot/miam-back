import { Cook } from './cook.entity';
import { CooksService } from './cooks.service';
import { CooksController } from './cooks.controller';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Cook])],
  controllers: [CooksController],
  providers: [CooksService],
  exports: [CooksService],
})
export class CooksModule {}
