import { ApiProperty } from '@nestjs/swagger';

export class UpdateCookDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  recipeId: number;

  @ApiProperty()
  ingredientId: number;

  @ApiProperty()
  unitId: number;
}
