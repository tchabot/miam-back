import { ApiProperty } from '@nestjs/swagger';

export class CreateCookDto {
  @ApiProperty()
  recipeId: number;

  @ApiProperty()
  ingredientId: number;

  @ApiProperty()
  unitId: number;
}
