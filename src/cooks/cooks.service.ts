import { Cook } from './cook.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CooksService {
  constructor(
    @InjectRepository(Cook)
    private cookRepository: Repository<Cook>,
  ) {}

  async create(data: any): Promise<Cook> {
    return await this.cookRepository.save(data);
  }

  async update(id: number, data: Object): Promise<Cook> {
    return await this.cookRepository.save({ id: id, ...data });
  }

  findAll(): Promise<Cook[]> {
    return this.cookRepository.find();
  }

  findOne(id: number): Promise<Cook> {
    return this.cookRepository.findOne(id);
  }

  findByRecipe(id: number): Promise<Cook[]> {
    return this.cookRepository
      .createQueryBuilder('cook')
      .where('cook.recipeId = :id', { id: id })
      .getMany();
  }

  async remove(id: number): Promise<void> {
    await this.cookRepository.delete(id);
  }
}
