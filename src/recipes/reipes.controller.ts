import { CreateRecipeDto } from './dto/create-recipe.dto';
import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import {
  Controller,
  Get,
  Delete,
  Body,
  Put,
  Param,
  UseGuards,
  Post,
} from '@nestjs/common';
import { RecipesService } from './recipes.service';
import { Recipe } from './recipe.entity';
import { UpdateRecipeDto } from './dto/update-recipe.dto';

@UseGuards(JwtAuthGuard)
@Controller('recipes')
export class RecipesController {
  constructor(private readonly recipesService: RecipesService) {}

  @Get()
  findAll(): Promise<Recipe[]> {
    return this.recipesService.findAll();
  }

  @Post()
  create(@Body() recipe: CreateRecipeDto): Promise<Recipe> {
    return this.recipesService.create(recipe);
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Recipe> {
    return this.recipesService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateRecipeDto: UpdateRecipeDto,
  ): Promise<Recipe> {
    return this.recipesService.update(parseInt(id), updateRecipeDto);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<void> {
    return this.recipesService.remove(id);
  }
}
