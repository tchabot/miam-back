import { ApiProperty } from '@nestjs/swagger';

export class UpdateRecipeDto {
  @ApiProperty()
  id: number;
  
  @ApiProperty()
  name: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  parts: number;
}
