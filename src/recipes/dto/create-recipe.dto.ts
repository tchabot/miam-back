import { Category } from './../../categories/category.entity';
import { ApiProperty } from '@nestjs/swagger';

export class CreateRecipeDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  description: string;

  @ApiProperty()
  parts: number;

  @ApiProperty()
  category: Category;
}
