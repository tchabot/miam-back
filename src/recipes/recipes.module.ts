import { Recipe } from './recipe.entity';
import { RecipesController } from './reipes.controller';
import { Module } from '@nestjs/common';
import { RecipesService } from './recipes.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Recipe])],
  controllers: [RecipesController],
  providers: [RecipesService],
  exports: [RecipesService],
})
export class RecipesModule {}
