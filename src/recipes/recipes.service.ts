import { Recipe } from './recipe.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class RecipesService {
  constructor(
    @InjectRepository(Recipe)
    private recipeRepository: Repository<Recipe>,
  ) {}

  async create(data: any): Promise<Recipe> {
    return await this.recipeRepository.save(data);
  }

  async update(id: number, data: Object): Promise<Recipe> {
    return await this.recipeRepository.save({ id: id, ...data });
  }

  findAll(): Promise<Recipe[]> {
    return this.recipeRepository.find({
      relations: ['categories'],
      order: { name: 'ASC' },
    });
  }

  findOne(id: number): Promise<Recipe> {
    return this.recipeRepository.findOne(id, { relations: ['categories'] });
  }

  async remove(id: number): Promise<void> {
    await this.recipeRepository.delete(id);
  }
}
