import { CooksModule } from './cooks/cooks.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { IngredientsModule } from './ingredients/ingredients.module';
import { RecipesModule } from './recipes/recipes.module';
import { ShopListModule } from './shop-list/shop-list.module';
import { UnitsModule } from './units/units.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { CategoriesModule } from './categories/categories.module';
import { UserListModule } from './user-list/user-list.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: '51.210.182.219',
      port: 5432,
      username: 'tchabot',
      password: 'toor*',
      database: 'miam',
      entities: ['dist/**/*.entity{.ts,.js}'],
      autoLoadEntities: true,
    }),
    IngredientsModule,
    RecipesModule,
    ShopListModule,
    UnitsModule,
    AuthModule,
    UsersModule,
    CooksModule,
    CategoriesModule,
    UserListModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
