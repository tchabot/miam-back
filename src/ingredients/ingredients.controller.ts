import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import {
  Controller,
  Get,
  Delete,
  Body,
  Put,
  Param,
  UseGuards,
  Post,
} from '@nestjs/common';
import { IngredientsService } from './ingredients.service';
import { Ingredient } from './ingredient.entity';
import { UpdateIngredientDto } from './dto/update-ingredient.dto';

@UseGuards(JwtAuthGuard)
@Controller('ingredients')
export class IngredientsController {
  constructor(private readonly ingredientsService: IngredientsService) {}

  @Get()
  findAll(): Promise<Ingredient[]> {
    return this.ingredientsService.findAll();
  }

  @Post()
  create(@Body() ingredient: Ingredient): Promise<Ingredient> {
    return this.ingredientsService.create(ingredient);
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Ingredient> {
    return this.ingredientsService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateIngredientDto: UpdateIngredientDto,
  ): Promise<Ingredient> {
    return this.ingredientsService.update(parseInt(id), updateIngredientDto);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<void> {
    return this.ingredientsService.remove(id);
  }
}
