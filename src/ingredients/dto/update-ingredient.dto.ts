import { ApiProperty } from '@nestjs/swagger';

export class UpdateIngredientDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  calories: number;

  @ApiProperty()
  water: number;

  @ApiProperty()
  proteins: number;

  @ApiProperty()
  glucids: number;

  @ApiProperty()
  lipids: number;

  @ApiProperty()
  sugars: number;

  @ApiProperty()
  starch: number;

  @ApiProperty()
  fibers: number;

  @ApiProperty()
  fatSaturated: number;

  @ApiProperty()
  fatMonoUnsaturated: number;

  @ApiProperty()
  fatPolyUnsaturated: number;

  @ApiProperty()
  cholesterol: number;

  @ApiProperty()
  salt: number;

  @ApiProperty()
  calcium: number;

  @ApiProperty()
  chlorure: number;

  @ApiProperty()
  copper: number;

  @ApiProperty()
  iron: number;

  @ApiProperty()
  iode: number;

  @ApiProperty()
  magnesium: number;

  @ApiProperty()
  manganese: number;

  @ApiProperty()
  phosophore: number;

  @ApiProperty()
  potassium: number;

  @ApiProperty()
  selenium: number;

  @ApiProperty()
  sodium: number;

  @ApiProperty()
  zinc: number;

  @ApiProperty()
  retinol: number;

  @ApiProperty()
  betaCarotene: number;

  @ApiProperty()
  vitaminD: number;

  @ApiProperty()
  vitaminE: number;

  @ApiProperty()
  vitaminK1: number;

  @ApiProperty()
  vitaminK2: number;

  @ApiProperty()
  vitaminC: number;

  @ApiProperty()
  vitaminB1: number;

  @ApiProperty()
  vitaminB2: number;

  @ApiProperty()
  vitaminB3: number;

  @ApiProperty()
  vitaminB5: number;

  @ApiProperty()
  vitaminB6: number;

  @ApiProperty()
  vitaminB9: number;

  @ApiProperty()
  vitaminB12: number;
}
