import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Ingredient } from './ingredient.entity';

@Injectable()
export class IngredientsService {
  constructor(
    @InjectRepository(Ingredient)
    private ingredientRepository: Repository<Ingredient>,
  ) {}

  async create(data: any): Promise<Ingredient> {
    return await this.ingredientRepository.save(data);
  }

  async update(id: number, data: Object): Promise<Ingredient> {
    return await this.ingredientRepository.save({ id: id, ...data });
  }

  findAll(): Promise<Ingredient[]> {
    return this.ingredientRepository.find({ order: { name: 'ASC' } });
  }

  findOne(id: number): Promise<Ingredient> {
    return this.ingredientRepository.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.ingredientRepository.delete(id);
  }
}
