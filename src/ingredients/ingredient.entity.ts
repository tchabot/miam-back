import { ColumnNumericTransformer } from './../ColumnNumericTransformer';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { ShopList } from 'src/shop-list/shop-list.entity';

@Entity()
export class Ingredient {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(
    type => ShopList,
    shopLists => shopLists.ingredient,
  )
  shopLists: ShopList[];

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  calories: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  water: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  proteins: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  glucids: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  lipids: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  sugars: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  starch: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  fibers: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  fatSaturated: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  fatMonoUnsaturated: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  fatPolyUnsaturated: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  cholesterol: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  salt: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  calcium: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  chlorure: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  copper: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  iron: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  iode: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  magnesium: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  manganese: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  phosophore: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  potassium: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  selenium: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  sodium: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  zinc: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  retinol: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  betaCarotene: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminD: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminE: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminK1: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminK2: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminC: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminB1: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminB2: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminB3: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminB5: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminB6: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminB9: number;

  @Column('decimal', {
    nullable: true,
    precision: 10,
    scale: 2,
    transformer: new ColumnNumericTransformer(),
  })
  vitaminB12: number;
}
