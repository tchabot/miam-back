import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserList } from './user-list.entity';

@Injectable()
export class UserListService {
  constructor(
    @InjectRepository(UserList)
    private userListRepository: Repository<UserList>,
  ) {}

  async create(data: any): Promise<UserList> {
    return await this.userListRepository.save(data);
  }

  async update(id: number, data: Object): Promise<UserList> {
    return await this.userListRepository.save({ id: id, ...data });
  }

  findAll(): Promise<UserList[]> {
    return this.userListRepository.find({ order: { title: 'ASC' } });
  }

  findOne(id: number): Promise<UserList> {
    return this.userListRepository.findOne(id);
  }

  findByUserId(id: number): Promise<UserList[]> {
    return this.userListRepository
      .createQueryBuilder('userList')
      .leftJoinAndSelect('userList.shopLists', 'shopLists')
      .leftJoinAndSelect('shopLists.ingredient', 'ingredient')
      .where({ user: { id: id } })
      .orderBy('userList.title', 'ASC')
      .getMany();
  }

  async remove(id: number): Promise<void> {
    await this.userListRepository.delete(id);
  }
}
