import { UserList } from './user-list.entity';
import { UserListService } from './user-list.service';
import { Module } from '@nestjs/common';
import { UserListController } from './user-list.controller';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  controllers: [UserListController],
  providers: [UserListService],
  imports: [TypeOrmModule.forFeature([UserList])],
  exports: [UserListService],
})
export class UserListModule {}
