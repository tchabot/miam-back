import { User } from './../users/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  Column,
} from 'typeorm';
import { ShopList } from 'src/shop-list/shop-list.entity';

@Entity()
export class UserList {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @ManyToOne(
    type => User,
    user => user.userLists,
  )
  user: User;

  @OneToMany(
    type => ShopList,
    shopLists => shopLists.userList,
  )
  shopLists: ShopList[];
}
