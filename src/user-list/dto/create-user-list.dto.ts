import { User } from './../../users/user.entity';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserListDto {
  @ApiProperty()
  user: User;

  @ApiProperty()
  title: string;
}
