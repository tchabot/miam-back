import { ApiProperty } from '@nestjs/swagger';
import { User } from 'src/users/user.entity';

export class UpdateUserListDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  user: User;

  @ApiProperty()
  title: string;
}
