import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import {
  Controller,
  Get,
  Delete,
  Body,
  Put,
  Param,
  UseGuards,
  Post,
  Query,
} from '@nestjs/common';
import { UserListService } from './user-list.service';
import { UserList } from './user-list.entity';
import { CreateUserListDto } from './dto/create-user-list.dto';
import { UpdateUserListDto } from './dto/update-user-list.dto';

@UseGuards(JwtAuthGuard)
@Controller('user-list')
export class UserListController {
  constructor(private readonly userListService: UserListService) {}

  @Get()
  findAll(@Query() query: any): Promise<UserList[]> | Promise<UserList> {
    if (query.userId) {
      return this.userListService.findByUserId(query.userId);
    } else {
      return this.userListService.findAll();
    }
  }

  @Post()
  create(@Body() userList: CreateUserListDto): Promise<UserList> {
    return this.userListService.create(userList);
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<UserList> {
    return this.userListService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateUserListDto: UpdateUserListDto,
  ): Promise<UserList> {
    return this.userListService.update(parseInt(id), updateUserListDto);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<void> {
    return this.userListService.remove(id);
  }
}
