import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import {
  Controller,
  Get,
  Delete,
  Body,
  Put,
  Param,
  UseGuards,
  Post,
  Query,
} from '@nestjs/common';
import { ShopListService } from './shop-list.service';
import { ShopList } from './shop-list.entity';
import { CreateShopListDto } from './dto/create-shopList.dto';
import { UpdateShopListDto } from './dto/update-shopList.dto';

@UseGuards(JwtAuthGuard)
@Controller('shop-list')
export class ShopListController {
  constructor(private readonly shopListService: ShopListService) {}

  @Get()
  findAll(@Query() query: any): Promise<ShopList[]> | Promise<ShopList> {
    if (query.userId && query.ingredientId) {
      return this.shopListService.findByUserAndIngredient(
        query.userId,
        query.ingredientId,
      );
    } else {
      return this.shopListService.findAll();
    }
  }

  @Post()
  create(@Body() shopList: CreateShopListDto): Promise<ShopList> {
    return this.shopListService.create(shopList);
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<ShopList> {
    return this.shopListService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateShopListDto: UpdateShopListDto,
  ): Promise<ShopList> {
    return this.shopListService.update(parseInt(id), updateShopListDto);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<void> {
    return this.shopListService.remove(id);
  }
}
