import { Unit } from './../../units/unit.entity';
import { Ingredient } from './../../ingredients/ingredient.entity';
import { User } from './../../users/user.entity';
import { ApiProperty } from '@nestjs/swagger';

export class CreateShopListDto {
  @ApiProperty()
  user: User;

  @ApiProperty()
  ingredient: Ingredient;

  @ApiProperty({ nullable: true })
  unit: Unit;

  @ApiProperty()
  checked: boolean;

  @ApiProperty({ nullable: true })
  quantity: number;
}
