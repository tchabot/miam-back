import { ApiProperty } from '@nestjs/swagger';

export class UpdateShopListDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  checked: boolean;

  @ApiProperty({ nullable: true })
  quantity: number;
}
