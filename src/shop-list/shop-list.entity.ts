import { UserList } from './../user-list/user-list.entity';
import { Unit } from './../units/unit.entity';
import { Ingredient } from './../ingredients/ingredient.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity()
export class ShopList {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  quantity: number;

  @ManyToOne(
    type => UserList,
    userList => userList.shopLists,
  )
  userList: UserList;

  @ManyToOne(
    type => Ingredient,
    ingredient => ingredient.shopLists,
  )
  ingredient: Ingredient;

  @ManyToOne(
    type => Unit,
    unit => unit.shopLists,
  )
  unit: Unit;

  @Column()
  checked: boolean;
}
