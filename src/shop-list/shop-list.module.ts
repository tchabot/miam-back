import { ShopList } from './shop-list.entity';
import { Module } from '@nestjs/common';
import { ShopListService } from './shop-list.service';
import { ShopListController } from "./shop-list.controller";
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  providers: [ShopListService],
  imports: [TypeOrmModule.forFeature([ShopList])],
  controllers: [ShopListController],
  exports: [ShopListService]
})
export class ShopListModule {}
