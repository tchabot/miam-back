import { CreateShopListDto } from './dto/create-shopList.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ShopList } from './shop-list.entity';

@Injectable()
export class ShopListService {
  constructor(
    @InjectRepository(ShopList)
    private shopListRepository: Repository<ShopList>,
  ) {}

  async create(data: CreateShopListDto): Promise<ShopList> {
    return await this.shopListRepository.save(data);
  }

  async update(id: number, data: Object): Promise<ShopList> {
    return await this.shopListRepository.save({ id: id, ...data });
  }

  findAll(): Promise<ShopList[]> {
    return this.shopListRepository.find({relations: ['ingredient', 'unit']});
  }

  findOne(id: number): Promise<ShopList> {
    return this.shopListRepository.findOne(id, {relations: ['ingredient', 'unit']});
  }

  findByUserAndIngredient(
    userId: number,
    ingredientId: number,
  ): Promise<ShopList> {
    let result = this.shopListRepository
      .createQueryBuilder('shopList')
      .leftJoinAndSelect('shopList.ingredient', 'ingredient')
      .leftJoinAndSelect('shopList.unit', 'unit')
      .leftJoinAndSelect('shopList.userList', 'userList')
      .where({ userList: { userId: userId }, ingredient: { id: ingredientId } })
      .getOne();

      return result;
  }

  async remove(id: number): Promise<void> {
    await this.shopListRepository.delete(id);
  }
}
