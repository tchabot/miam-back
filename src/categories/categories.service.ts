import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Category } from './category.entity';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
  ) {}

  async create(data: any): Promise<Category> {
    return await this.categoryRepository.save(data);
  }

  async update(id: number, data: Object): Promise<Category> {
    return await this.categoryRepository.save({ id: id, ...data });
  }

  findAll(): Promise<Category[]> {
    return this.categoryRepository.find({ order: { name: 'ASC' } });
  }

  findOne(id: number): Promise<Category> {
    return this.categoryRepository.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.categoryRepository.delete(id);
  }
}
