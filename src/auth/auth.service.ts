import { User } from './../users/user.entity';
import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
const bcrypt = require('bcrypt');

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.findByEmail(email);

    if (user) {
      const match = await bcrypt.compare(pass, user.password);

      if (match) {
        const { password, ...result } = user;
        return result;
      }
    }

    return null;
  }

  async login(credentials: User) {
    const user = await this.validateUser(
      credentials.email,
      credentials.password,
    );

    if (user) {
      const payload = { id: user.id, email: user.email, sub: user.id };
      
      return {
        access_token: this.jwtService.sign(payload),
      };
    }

    return null;
  }
}
