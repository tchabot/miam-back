import { UserList } from './../user-list/user-list.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @OneToMany(
    type => UserList,
    userLists => userLists.user,
  )
  userLists: UserList[];
}
