import { User } from './user.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
const bcrypt = require('bcrypt');

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async create(data: any): Promise<User> {
    const hash = await bcrypt.hash(data.password, 10);
    data.password = hash;
    return await this.usersRepository.save(data);
  }

  async update(id: number, data: Object): Promise<User> {
    return await this.usersRepository.save({ id, ...data });
  }

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(id: number): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  async findByEmail(email: string): Promise<User | undefined> {
    return this.usersRepository.findOne({ email: email });
  }

  async remove(id: number): Promise<void> {
    await this.usersRepository.delete(id);
  }
}
