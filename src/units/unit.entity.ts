import { ShopList } from "src/shop-list/shop-list.entity";
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";

@Entity()
export class Unit {
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    name: string;

    @OneToMany(
        type => ShopList,
        shopLists => shopLists.unit,
      )
      shopLists: ShopList[];
}