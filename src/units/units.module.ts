import { Unit } from './unit.entity';
import { UnitsController } from './units.controller';
import { Module } from '@nestjs/common';
import { UnitsService } from './units.service';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Unit])],
  controllers: [UnitsController],
  providers: [UnitsService],
  exports: [UnitsService],
})

export class UnitsModule {}
