import { ApiProperty } from '@nestjs/swagger';

export class UpdateUnitDto {
  @ApiProperty()
  id: number;

  @ApiProperty()
  name: string;
}
