import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Unit } from './unit.entity';

@Injectable()
export class UnitsService {
  constructor(
    @InjectRepository(Unit)
    private unitRepository: Repository<Unit>,
  ) {}

  async create(data: any): Promise<Unit> {
    return await this.unitRepository.save(data);
  }

  async update(id: number, data: Object): Promise<Unit> {
    return await this.unitRepository.save({ id: id, ...data });
  }

  findAll(): Promise<Unit[]> {
    return this.unitRepository.find({ order: { name: 'ASC' } });
  }

  findOne(id: number): Promise<Unit> {
    return this.unitRepository.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.unitRepository.delete(id);
  }
}
