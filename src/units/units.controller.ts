import { JwtAuthGuard } from './../auth/jwt-auth.guard';
import {
  Controller,
  Get,
  Delete,
  Body,
  Put,
  Param,
  UseGuards,
  Post,
} from '@nestjs/common';
import { UnitsService } from './units.service';
import { Unit } from './unit.entity';
import { UpdateUnitDto } from './dto/update-unit.dto';

@UseGuards(JwtAuthGuard)
@Controller('units')
export class UnitsController {
  constructor(private readonly unitsService: UnitsService) {}

  @Get()
  findAll(): Promise<Unit[]> {
    return this.unitsService.findAll();
  }

  @Post()
  create(@Body() unit: Unit): Promise<Unit> {
    return this.unitsService.create(unit);
  }

  @Get(':id')
  findOne(@Param('id') id: number): Promise<Unit> {
    return this.unitsService.findOne(id);
  }

  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateUnitDto: UpdateUnitDto,
  ): Promise<Unit> {
    return this.unitsService.update(parseInt(id), updateUnitDto);
  }

  @Delete(':id')
  delete(@Param('id') id: number): Promise<void> {
    return this.unitsService.remove(id);
  }
}
